#include <gtest/gtest.h>
extern "C" {
  #include "circular_buffer.h"
}

TEST(CIRCULAR_BUFFER, TestAddingElements)
{
  uint8_t value;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(uint8_t i=0; i<10; i++)
  {
    c_buffer_add(c_buf, i);
  }

  value = c_buffer_get(c_buf, 2);

  c_buffer_destroy(c_buf);
  EXPECT_EQ(value, 2);
}

TEST(CIRCULAR_BUFFER, TestRemovingElemnts)
{
  uint8_t value;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(uint8_t i=0; i<10; i++)
  {
    c_buffer_add(c_buf, i);
  }

  c_buffer_pop(c_buf);
  c_buffer_pop(c_buf);

  value = c_buffer_get(c_buf, 0);

  c_buffer_destroy(c_buf);
  ASSERT_EQ(value, 2);
}


TEST(CIRCULAR_BUFFER, TestGetFromBack)
{
  uint8_t value;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(uint8_t i=0; i<10; i++)
  {
    c_buffer_add(c_buf, i);
  }

  value = c_buffer_get_back(c_buf, 0);

  c_buffer_destroy(c_buf);
  ASSERT_EQ(value, 9);
}

TEST(CIRCULAR_BUFFER, TestGetFromBackLastElements)
{
  uint8_t value;
  uint8_t i;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(i=10; i<20; i++)
  {
    c_buffer_add(c_buf, i);
  }

  value = c_buffer_get_back(c_buf, 9);

  c_buffer_destroy(c_buf);
  ASSERT_EQ(value, 10);
}

TEST(CIRCULAR_BUFFER, TestCountElement)
{
  uint8_t count, i;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(i=0; i<10; i++)
  {
    c_buffer_add(c_buf, i);
  }

  count = c_buffer_count(c_buf);

  c_buffer_destroy(c_buf);
  ASSERT_EQ(count, 10);
}

TEST(CIRCULAR_BUFFER, TestCountElementAfterRemovingElements)
{
  uint8_t count, i;
  cbuf_handle_t c_buf =	c_buffer_create(100);
  
  for(i=0; i<10; i++)
  {
    c_buffer_add(c_buf, i);
  }

  //remove 2 elements
  c_buffer_pop(c_buf);
  c_buffer_pop(c_buf);

  count = c_buffer_count(c_buf);

  c_buffer_destroy(c_buf);
  ASSERT_EQ(count, 8);
}

TEST(CIRCULAR_BUFFER, TestAddingElementsAfterBufforIsFull)
{
  uint8_t last, i;
  cbuf_handle_t c_buf =	c_buffer_create(10);  // create smaller buffer

  //add 5 elements more than buffer capacity
  for(i=0; i<15; i++)
  {
    c_buffer_add(c_buf, i);
  }

  last = c_buffer_get_back(c_buf, 0);
  c_buffer_destroy(c_buf);

  ASSERT_EQ(last, 14);
}

TEST(CIRCULAR_BUFFER, TestFirstElementAddingElementsAfterBufforIsFull)
{
  uint8_t first, i;
  cbuf_handle_t c_buf =	c_buffer_create(10);  // create smaller buffer

  //add 5 elements more than buffer capacity
  for(i=0; i<15; i++)
  {
    c_buffer_add(c_buf, i);
  }

  first = c_buffer_get(c_buf, 0);
  c_buffer_destroy(c_buf);

  ASSERT_EQ(first, 5);
}

TEST(CIRCULAR_BUFFER, TestCountElementAddingElementsAfterBufforIsFull)
{
  uint8_t count, i;
  cbuf_handle_t c_buf =	c_buffer_create(10);  // create smaller buffer

  //add 5 elements more than buffer capacity
  for(i=0; i<15; i++)
  {
    c_buffer_add(c_buf, i);
  }

  count = c_buffer_count(c_buf);
  c_buffer_destroy(c_buf);

  ASSERT_EQ(count, 10);
}

TEST(CIRCULAR_BUFFER, AddingElementsAfterPop)
{
  uint8_t count, i;
  cbuf_handle_t c_buf =	c_buffer_create(100);  // create smaller buffer
  uint8_t values[3];

  //add 5 elements more than buffer capacity
  for(i=0; i<15; i++)
  {
    c_buffer_add(c_buf, i);
  }

  for(i=0; i<5; i++)
  {
    c_buffer_pop(c_buf);
  }

  c_buffer_add(c_buf, 35);
  c_buffer_add(c_buf, 45);

  count = c_buffer_count(c_buf);

  values[0] = c_buffer_get_back(c_buf, 0);
  values[1] = c_buffer_get_back(c_buf, 1);
  values[2] = c_buffer_get_back(c_buf, 2);

  c_buffer_destroy(c_buf);

  ASSERT_EQ(count, 12);
  ASSERT_EQ(values[0], 45);
  ASSERT_EQ(values[1], 35);
  ASSERT_EQ(values[2], 14);
}
