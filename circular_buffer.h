/*
 * circular_buffer.h
 *
 *  Created on: Nov 11, 2021
 *      Author: robsk
 */

#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_

#include <stddef.h>
#include <stdint.h>

/// @brief Circular buffer struct.
typedef struct circular_buffer *cbuf_handle_t;

#ifdef __cplusplus
extern "C" {
#endif

// Creating / destroying object
cbuf_handle_t c_buffer_create(uint16_t elements_count);
void c_buffer_destroy(cbuf_handle_t c_buf);

// Manipulate collection
void c_buffer_push(cbuf_handle_t c_buf, uint8_t data);
uint8_t c_buffer_pop(cbuf_handle_t c_buf);
uint8_t c_buffer_get(cbuf_handle_t c_buf, uint16_t index);
uint8_t c_buffer_get_back(cbuf_handle_t c_buf, uint16_t index);

// Check data about collection
uint8_t c_buffer_empty(cbuf_handle_t c_buf);
uint8_t c_buffer_full(cbuf_handle_t c_buf);
size_t c_buffer_count(cbuf_handle_t c_buf);

void c_buffer_get_back_range(cbuf_handle_t c_buf, uint8_t *dest, uint16_t index, size_t cout);

#ifdef __cplusplus
}
#endif

#endif /* _CIRCULAR_BUFFER_H_ */
