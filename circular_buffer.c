/*
 * circular_buffer.c
 *
 *  Created on: Nov 11, 2021
 *      Author: robsk
 */
#include "circular_buffer.h"

#include <stdlib.h>
#include <string.h>

/// @brief Circular buffer data.
struct circular_buffer{
	uint8_t * buffer; 	///< Data buffer.
	size_t head;		///< First added element.
	size_t tail;		///< Last added element.
	size_t capacity; 	///< Capacity of collection.
	size_t count;		///< Size of collection.
	uint8_t is_empty;	///< If collection is empty
	uint8_t is_full;	///< If collection if full.
};

/**
 * @brief Create Circular buffer
 * 
 * @param size 	Size of buffer;
 * @return cbuf_handle_t Circular buffer object.
 */
cbuf_handle_t c_buffer_create(uint16_t size)
{
	cbuf_handle_t c_buf = malloc(sizeof(struct circular_buffer));
	while(c_buf == NULL);

	c_buf->buffer = malloc(size);
	while(c_buf->buffer == NULL);

	c_buf->head 	= c_buf->tail = 0;
	c_buf->capacity	= size;
	c_buf->is_empty = 1;
	c_buf->is_full	= 0;
	c_buf->count	= 0;
	return c_buf;
}

/**
 * @brief Remove circular buffer object.
 * 
 * @param c_buf Circular buffer object to destroy.
 */
void c_buffer_destroy(cbuf_handle_t c_buf)
{
	if(c_buf != NULL)
	{
		free(c_buf->buffer);
		free(c_buf);
	}
}

/**
 * @brief Add element to collection.
 * 
 * @param c_buf Circular buffer object.
 * @param value Element to add.
 */
void c_buffer_push(cbuf_handle_t c_buf, uint8_t value)
{
	c_buf->buffer[ c_buf->tail ] = value;
	c_buf->tail++;

	if(c_buf->tail >= c_buf->capacity ) c_buf->tail 	= 0;

	if(c_buf->is_full)					c_buf->head		= c_buf->tail;	// move head to be equal tail, lose byte
	else								c_buf->count++;

	if(c_buf->head == c_buf->tail )
	{
		c_buf->is_full 	= 1;
	}
	if(c_buf->is_empty) 				c_buf->is_empty = 0;
}

/**
 * @brief Remove last element from collection.
 * 	
 * @param c_buf Circular buffer object.
 * @return uint8_t Element.
 */
uint8_t c_buffer_pop(cbuf_handle_t c_buf)
{
	uint8_t result = c_buf->buffer[ c_buf->head ];

	c_buf->head++;
	c_buf->count--;

	if(c_buf->head >= c_buf->capacity ) c_buf->head 	= 0;

	if(c_buf->head == c_buf->tail ) 	c_buf->is_empty = 1;
	if(c_buf->is_full)					c_buf->is_full 	= 0;

	return result;
}

/**
 * @brief Check if there aren't any element in collection.
 * 
 * @param c_buf Circular buffer object.
 * @retval	0	Collection not empty.
 * @retval	1	Collection empty.
 */
inline uint8_t c_buffer_empty(cbuf_handle_t c_buf)
{
	return c_buf->is_empty;
}

/**
 * @brief Check if buffer is full.
 * 
 * @param c_buf 	Circular buffer object.
 * @retval	0		Buffer not full.
 * @retval	1		Buffer full. 
 */
inline uint8_t c_buffer_full(cbuf_handle_t c_buf)
{
	return c_buf->is_full;
}

/**
 *  Get element by `index` from begining (head)
 */
uint8_t c_buffer_get(cbuf_handle_t c_buf, uint16_t index)
{
	uint16_t true_index = c_buf->head + index;

	if(true_index > c_buf->capacity) true_index -= c_buf->capacity;

	return c_buf->buffer[true_index];
}

/**
 *  Get element by `index` from end (tail)
 */
uint8_t c_buffer_get_back(cbuf_handle_t c_buf, uint16_t index)
{
	if(c_buf->tail > index) 	return c_buf->buffer[ c_buf->tail - index  - 1];
	else						return c_buf->buffer[ c_buf->capacity - index + c_buf->tail ];
}

/**
 * 	Get how many element are in array
 */
inline size_t c_buffer_count(cbuf_handle_t c_buf)
{
	return c_buf->count;
}

void c_buffer_get_back_range(cbuf_handle_t c_buf, uint8_t *dest, uint16_t index, size_t count)
{
	if(c_buf->tail >= index)
	{
		memcpy(dest, c_buf->buffer + c_buf->tail - index - count, count);
	}
	else
	{
		size_t to_end =  c_buf->tail - index ;

		memcpy(dest, c_buf->buffer + c_buf->capacity - c_buf->tail + index, to_end );
		memcpy(dest +  count - to_end, c_buf->buffer, count - to_end );
	}
}
